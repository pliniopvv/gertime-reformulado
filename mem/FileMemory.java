/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mem;

import obj.Estagiario;
import java.io.File;
import java.io.IOException;
import java.util.zip.ZipException;
import win.MZip;

/**
 *
 * @author Plinio
 */
public class FileMemory extends Memory {

    //DIRS
    private static final String dirEstagiarios = "Estagiarios";
    //PROPRIEDADES
    private File file;
    private MZip zip;

    public FileMemory(Memory memoria) {
        setEstagiarios(memoria.getEstagiarios());
    }

    public FileMemory(File file) throws ZipException, IOException, ClassNotFoundException {
        this.file = file;
        zip = new MZip(file);
        if (file.exists()) {
            load(zip);
        }
    }

    public FileMemory(MZip zip) {
        this.zip = zip;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void load(MZip zip) throws ClassNotFoundException, IOException {
        setEstagiarios((Estagiario[]) zip.readObject(dirEstagiarios));
    }

    public void save() throws IOException {
        zip.saveObject(getEstagiarios(), dirEstagiarios);
    }
}
