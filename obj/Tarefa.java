/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package obj;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Plinio
 */
public class Tarefa implements Serializable {

    private static int GlobalId;
    private int ID;
    private Boolean status;
    private String titulo;
    private String descricao;
    private Date startData;

    /*
     * Construtores
     */
    public Tarefa(String titulo, String descricao, Boolean status) {
        setID();
        this.status = status;
        this.titulo = titulo;
        this.descricao = descricao;
        this.startData = new Date();
    }

    public Tarefa(String titulo, String descricao) {
        setID();
        this.status = false;
        this.titulo = titulo;
        this.descricao = descricao;
        this.startData = new Date();
    }

    /*
     * Indexadores
     */
    private void setID() {
        if (!(Tarefa.GlobalId >= 0)) {
            this.ID = 0;
            Tarefa.GlobalId = 1;
        } else {
            this.ID = Tarefa.GlobalId;
            Tarefa.GlobalId++;
        }
    }

    private int getID() {
        return this.ID;
    }

    /*
     * Getters e Setters automáticos
     */
    public String getData() {
        SimpleDateFormat fDate = new SimpleDateFormat("d/M/y h:m:s");
        return fDate.format(startData);
    }

    public void setData(Date data) {
        this.startData = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
