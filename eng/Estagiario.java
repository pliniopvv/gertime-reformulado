/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eng;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Plinio
 */
public class Estagiario implements Serializable {

    private ArrayList<Tarefa> livroTarefas = new ArrayList<>();
    private String nome;

    public Estagiario(String nome) {
        this.nome = nome;
    }

    /*
     * Necessários
     */
    public Tarefa[] getTarefas() {
        return livroTarefas.toArray(new Tarefa[livroTarefas.size()]);
    }

    public void addTarefa(Tarefa tarefa) {
        livroTarefas.add(tarefa);
    }

    public void removeTarefa(int tarefaIndex) {
        removeTarefa(livroTarefas.get(tarefaIndex));
    }

    public void removeTarefa(Tarefa tarefa) {
        livroTarefas.remove(tarefa);
    }

    /*
     * Getters & Setters automáticos.
     */
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
