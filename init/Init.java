/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package init;

import form.GerTime;
import mem.Memory;

/**
 *
 * @author Plinio
 */
public class Init {

    public static void main(String[] args) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows Classic".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Falha em carregar o Look And Feel");
            ex.printStackTrace();
        }
        
        
        
        // criar arquivo para carregar Memory!
        GerTime a = new GerTime();
        a.start();
    }
}
